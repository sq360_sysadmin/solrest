#Solrest

##Description 

Solrest exposes Rest resources for each Search API Solr Index defined on the site.

## Instructions

The module will automatically create Rest resource for every Solr Index defined on the site. These must be enabled, 
configured individually and assigned appropriate permissions. The restui module will help you do this.

### Removing rest endpoints

If you delete a search index before disabling the Rest endpoint you will have to remove the rest endpoint manually.  
```
drush cdel rest.resource.solrest_resource.name_ofIndex
```

## Use case
If you have a front end application which needs to communicate with Solr directly this module will allow you do so 
without interference from Search API. Queries to the Rest endpoint will be passed (more or less) straight to the Solr 
server and the data returned as is.

## Alternatives
Search API provides a Rest resource for all it's indices. It will return Drupal rendered cached Drupal entities.

You may also want to create a custom API which accesses Solr directly using Solarium or Search API.  


